<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <!-- Custom CSS -->
  <link rel="stylesheet" href="<?= base_url() ?>/public/asset/css/style.css">

  <!-- Font Awesome CSS-->
  <script src="https://use.fontawesome.com/c425454797.js"></script>

  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;700;800&display=swap" rel="stylesheet">

  <title>LangitPay Belajar</title>
</head>

<body>
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-transparent py-4 w-100">
    <div class="container">
      <a class="navbar-brand" href="#">Logo Here</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto mr-lg-3">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Kategori
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">Android</a>
              <a class="dropdown-item" href="#">iOS</a>
              <a class="dropdown-item" href="#">Web</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link mx-lg-2" href="#">Tentang Kami</a>
          </li>
        </ul>
        <a class="btn btn-success btn-brand btn-rounded px-4 ml-lg-4 text-uppercase" id="btn-read" href="#">
          Belajar <i class="fa fa-arrow-right ml-1" id="arrow-right"></i>
        </a>
      </div>
    </div>
  </nav>
  <!-- End Navbar -->

  <!-- Welcome -->
  <header class="page-header page-header-dark bg-gradient-primary-to-secondary">
    <div class="py-5">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6">
            <span class="landing-heading-text">Tingkatkan Skillmu, Ayo Belajar.</span>
            <p class="landing-subheading-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus atque omnis aut facilis vitae optio illum impedit nesciunt?</p>
            <a class="btn btn-success btn-brand btn-rounded px-5 py-3 mt-3 text-uppercase" id="btn-read-landing" href="#">
              Belajar Sekarang
            </a>
          </div>
          <div class="col-lg-6">
            <img src="<?= base_url() ?>/public/asset/img/reading.svg" class="img-fluid" id="img-welcome">
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- End Welcome -->

  <!-- Info Panel -->
  <div class="container" id="info-panel-position">
    <div class="row justify-content-center">
      <div class="col-10 info-panel p-3">
        <div class="row">
          <div class="col-lg">
            <img src="<?= base_url() ?>/public/asset/img/1-100.jpg" class="float-left mr-3">
            <h4>Android</h4>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
          </div>
          <div class="col-lg">
            <img src="<?= base_url() ?>/public/asset/img/2-100.jpg" class="float-left mr-3">
            <h4>iOS</h4>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
          </div>
          <div class="col-lg">
            <img src="<?= base_url() ?>/public/asset/img/3-100.jpg" class="float-left mr-3">
            <h4>Web</h4>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Info Panel -->

  <!-- Top Views -->
  <div class="container" id="top-views">
    <div class="row mb-5">
      <div class="col-lg text-center">
        <p class="section-subheading">Tersedia beragam materi yang dapat kamu akses secara gratis.</p>
        <h2 class="section-heading">Materi Unggulan</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 ">
        <div class="card">
          <img src="https://via.placeholder.com/200x250" class="card-img-top">
          <div class="card-body">
            <span class="badge bg-android mb-3">Android</span>
            <h5 class="card-title">Belajar Fundamental Aplikasi Android</h5>
          </div>
          <div class="card-footer p-0">
            <a href="#" class="btn btn-success btn-learn w-100">Belajar</a>
          </div>
        </div>
      </div>
      <div class="col-lg-3 ">
        <div class="card">
          <img src="https://via.placeholder.com/200x250" class="card-img-top">
          <div class="card-body">
            <span class="badge bg-web mb-3">Web</span>
            <h5 class="card-title">Membangun Progressive Web Apps</h5>
          </div>
          <div class="card-footer p-0">
            <a href="#" class="btn btn-success btn-learn w-100">Belajar</a>
          </div>
        </div>
      </div>
      <div class="col-lg-3 ">
        <div class="card">
          <img src="https://via.placeholder.com/200x250" class="card-img-top">
          <div class="card-body">
            <span class="badge bg-ios mb-3">iOS</span>
            <h5 class="card-title">Memulai Pemrograman Dengan Swift</h5>
          </div>
          <div class="card-footer p-0">
            <a href="#" class="btn btn-success btn-learn w-100">Belajar</a>
          </div>
        </div>
      </div>
      <div class="col-lg-3 ">
        <div class="card">
          <img src="https://via.placeholder.com/200x250" class="card-img-top">
          <div class="card-body">
            <span class="badge bg-web mb-3">Web</span>
            <h5 class="card-title">Belajar Dasar Pemrograman Web</h5>
          </div>
          <div class="card-footer p-0">
            <a href="#" class="btn btn-success btn-learn w-100">Belajar</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Top Views -->

  <!-- Category -->
  <div class="container" id="category">
    <div class="row mb-5">
      <div class="col-lg">
        <p class="section-subheading">Punya banyak skill memang sedang dibutuhkan industri saat ini lho.</p>
        <h2 class="section-heading">Beragam Kategori</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <div class="category-big-panel">
          <img src="<?= base_url() ?>/public/asset/img/category.svg" class="img-fluid">
        </div>
      </div>
      <div class="col-lg-6" id="category-panel">
        <div class="row">
          <div class="col category-item bg-android m-1">
            <p class="category-title">Android</p>
            <a href="#" class="btn btn-success btn-rounded btn-category px-4 float-right">Lanjut</a>
          </div>
          <div class="col category-item bg-ios m-1">
            <p class="category-title">iOS</p>
            <a href="#" class="btn btn-success btn-rounded btn-category px-4 float-right">Lanjut</a>
          </div>
        </div>
        <div class="row">
          <div class="col category-item bg-web m-1">
            <p class="category-title">Web</p>
            <a href="#" class="btn btn-success btn-rounded btn-category px-4 float-right">Lanjut</a>
          </div>
          <div class="col category-item bg-brand m-1">
            <p class="category-title">Lainnya</p>
            <a href="#" class="btn btn-success btn-rounded btn-category px-4 float-right">Lanjut</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Category -->

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>